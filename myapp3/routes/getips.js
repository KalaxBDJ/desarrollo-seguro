var express = require('express');
var router = express.Router();
const axios = require('axios').default;

//para mongo
const ip = require("../models/ip");


/**
 * Function to get data from external endpoints and compare every single IP in order to filter the ones in database
 */
router.get('/', function(req, res, next) {
    axios
    .get('http://192.168.49.2:31110/').then(response => {
      var result = response.data.split("\n")
      var excludeIp = [];
      ip.find({}, (err, ips) => {
        if (err) return res.status(500).send({ message: `Error al realizar la peticion: ${err}` })
        if (!ips) return res.status(404).send({ message: 'No existen productos' })
        ips.forEach(ip => {
          if(ip.ip.trim() != ""){
            excludeIp.push(ip.ip);
          }
        });
        const filterip = result.filter(resul => { 
          if(excludeIp.includes(resul)){
            return false;
          }else{
            return true; 
          }
        });
        res.send(filterip);

      })
      
      
    })
    .catch(error => {
      res.send({
        status : 'failed',
        message : 'Error al obtener las ips',
        data : []
      });
    });
    
  });
module.exports = router;
