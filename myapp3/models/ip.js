const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const IpSchema = Schema({
    ip: String,
});

module.exports = mongoose.model('Ip', IpSchema);