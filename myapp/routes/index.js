var express = require('express');
const axios = require('axios');
const https = require('https');

//Require NodeCache
const NodeCache = require("node-cache");
const myCache = new NodeCache();

const { response, json } = require('express');
var router = express.Router();

/* GET ip list. */
router.get('/', function (req, res, next) {

  // At request level
  const agent = new https.Agent({
    rejectUnauthorized: false
  });

  //Validate if key alreaddy exists in cache
  if (myCache.get("ips") !== undefined) {
    res.send(myCache.get("ips"));
  }
  else {
    axios
      .get('https://check.torproject.org/torbulkexitlist', { httpsAgent: agent })
      .then(response => {
        const ips1 = {
          status: 'ok',
          msg: 'ok',
          data: response.data.split("\n")
        };

        //Set cache and send response
        myCache.set("ips", ips1, 1920);
        res.send(ips1);
      })
      .catch(error => {
        res.send({
          status: 'failed',
          msg: 'Request Error, Try again',
          data: []
        });
      });
  }
});

module.exports = router;
