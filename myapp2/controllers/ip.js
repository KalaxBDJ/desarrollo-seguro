const Ip = require("../models/ip");


function saveIp(req, res) {
    var ip = new Ip();
    ip.ip = req.body.ip;
    if(ip.ip.match(/^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}$/gm)){
        ip.save((err, ipStored) => {
            if (err) res.status(500).send({message : `Error saving IP: ${err}`});

            res.status(200).send({ip: ipStored});
        })
    }else {
        res.status(400).send({message: "It is not an IP"});
    } 
}

module.exports = {
    saveIp
}