var express = require('express');
var router = express.Router();
const axios = require('axios').default;

/* GET users listing. */
router.get('/', function(req, res, next) {
  axios
  .get('http://192.168.49.2:31110')
  .then(response => {
    res.send({
      status : 'ok',
      message : 'You are only receiving 1 ip, testing purposes',
      data : response.data.data[0]
    });
  })
  .catch(error => {
    res.send({
      status : 'failed',
      message : 'Try again',
      data : []
    });
  });
});

module.exports = router;
