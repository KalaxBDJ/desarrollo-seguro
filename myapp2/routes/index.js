var express = require('express');
const { response } = require('../app');
const ipCtlr = require('../controllers/ip');
var router = express.Router();
//const axios = require('axios').default;

// router.post('/blacklist', (req, res) => {
//   res.send(req.body.ip);
// });

router.post('/blacklist', ipCtlr.saveIp);

module.exports = router;
